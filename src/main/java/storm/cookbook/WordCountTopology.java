package storm.cookbook;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.utils.Utils;

public class WordCountTopology {
	public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException, AuthorizationException {
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("readFileSpout", new ReadFileSpout());
		builder.setBolt("splitWordBolt", new SplitWordBolt()).shuffleGrouping("readFileSpout");
		builder.setBolt("countBolt", new CountBolt()).fieldsGrouping("splitWordBolt", new Fields("word"));
		Config conf = new Config();
		conf.setDebug(false);
		
		if(args.length > 1) {
			conf.put("wordsFile", args[1]);
			conf.setNumWorkers(1);
			StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
			
		} else {
			conf.put("wordsFile", "src/main/resources/words.txt");
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("HellworldTopology", conf, builder.createTopology());
			Utils.sleep(10000);
			cluster.shutdown();
		}
	}
}
