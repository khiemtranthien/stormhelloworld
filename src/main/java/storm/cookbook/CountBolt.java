package storm.cookbook;

import java.util.HashMap;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountBolt extends BaseRichBolt {
	private static final long serialVersionUID = 5744335322756330936L;
	
	private final Logger LOG = LoggerFactory.getLogger(CountBolt.class);
	
	Map<String, Integer> counters;

	OutputCollector collector;
	
	Integer id;
	String name;
	
	/**
	* At the end of the spout (when the cluster is shutdown
	* We will show the word counters
	*/
	@Override
	public void cleanup() {
		LOG.info("-- Word Counter [" + name +" - " + id + "] --");
		for(Map.Entry<String, Integer> entry : counters.entrySet()){
			LOG.info(entry.getKey() + ": " + entry.getValue());
		}
	}
	
	@Override
	public void execute(Tuple input) {
		String str = input.getString(0);
		/**
		 * If the word dosn't exist in the map we will create this, if not We
		 * will add 1
		 */
		if (!counters.containsKey(str)) {
			counters.put(str, 1);
		} else {
			Integer c = counters.get(str) + 1;
			counters.put(str, c);
		}
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.counters = new HashMap<String, Integer>();
		this.name = context.getThisComponentId();
		this.id = context.getThisTaskId();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		
	}
}
