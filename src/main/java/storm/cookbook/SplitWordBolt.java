package storm.cookbook;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class SplitWordBolt extends BaseRichBolt {
	OutputCollector collector;

	private static final long serialVersionUID = 8251135323475321397L;

	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void execute(Tuple input) {
		String sentence = input.getStringByField("sentence");
		if (sentence != null && !sentence.isEmpty()) {
			String[] words = sentence.split(" ");
			for (String word : words) {
				word = word.trim();
				if (!word.isEmpty()) {
					word = word.toLowerCase();
					// Emit the word
					collector.emit(new Values(word));
				}
			}
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
